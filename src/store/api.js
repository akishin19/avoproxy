export default class API {
    constructor(apiKey) {
        this.apiURL = `https://proxy6.net/api/${apiKey}`;
        this.getContentFromUrl = `https://proxy.piar4u.com/getContentFromUrl.php`;
    }
    async sendRequest(method, requestBody) {
        let response = await fetch(`${this.getContentFromUrl}?ext_url=${this.apiURL}/${method}?${requestBody}`);
        let json = JSON.parse(await response.json());
        decorateProxyValues(json);
        this.balance = json.balance;
        return json;
    }
    async getProxy(state = "all", descr = "") {
        let requestBody = `state=${state}%26descr=${descr}`;
        let json = await this.sendRequest("getproxy", requestBody);
        this.proxyList = json.list;
        return json;
    }
    async getPrice(count, period, version = "6") {
        let requestBody = `count=${count}%26period=${period}%26version=${version}`;
        let json = await this.sendRequest("getprice", requestBody);
        return json;
    }
    async getCount(country, version = "6") {
        let requestBody = `country=${country}%26version=${version}`;
        let json = await this.sendRequest("getcount", requestBody);
        return json;
    }
    async getCountry(version = "6") {
        let requestBody = `version=${version}`;
        let json = await this.sendRequest("getcountry", requestBody);
        return json;
    }
    async setType(ids, type) {
        let requestBody = `ids=${ids}%26type=${type}`;
        let json = await this.sendRequest("settype", requestBody);
        return json;
    }
    async buy(count, period, country, version = "6", type = "http") {
        let requestBody = `count=${count}%26period=${period}%26country=${country}%26version=${version}%26type=${type}`;
        let json = await this.sendRequest("buy", requestBody);
        return json;
    }
    async prolong(period, ids) {
        let requestBody = `period=${period}%26ids=${ids}`;
        let json = await this.sendRequest("prolong", requestBody);
        for (let item in json.list) {
            this.proxyList[item].TTL = json.list[item].TTL;
            this.proxyList[item].date_end = json.list[item].date_end;
        }
        return json;
    }
    async setDescr(newComment, ids) {
        let requestBody = `new=${newComment}%26ids=${ids}`;
        let json = await this.sendRequest("setdescr", requestBody);
        return json;
    }
    async delete(ids) {
        let requestBody = `ids=${ids}`;
        let json = await this.sendRequest("delete", requestBody);
        return json;
    }
    async check(ids) {
        let requestBody = `ids=${ids}`;
        let json = await this.sendRequest("check", requestBody);
        return json;
    }
}

function getTimeToExpiration(stringDate) {
    let dateNow = new Date();
    let expirationDate = Date.parse(stringDate);
    let difference = expirationDate - dateNow;
    let msMinute = 1000 * 60;
    let msInHour = 1000 * 60 * 60;
    let fullHours = Math.floor(difference / msInHour);
    let fullMinutes = Math.floor(difference / msMinute);
    let days = Math.floor(fullHours / 24);
    let hours = (days === 0 ? Math.floor(fullHours) : Math.floor(fullHours - 24 * days));
    if (fullHours < 0) return [`0d 0h`, fullHours, fullMinutes];
    return [`${days}d ${hours}h`, fullHours, fullMinutes];
}

function transformDate(stringDate) {
    let date = new Date(Date.parse(stringDate));
    let month = parseInt(date.getMonth()) + 1;
    let day = parseInt(date.getDate());
    let hour = parseInt(date.getHours());
    let minute = parseInt(date.getMinutes());
    if (month < 10) {
        month = `0` + month;
    }
    if (day < 10) {
        day = `0` + day;
    }
    if (hour < 10) {
        hour = `0` + hour;
    }
    if (minute < 10) {
        minute = `0` + minute;
    }
    return `${day}-${month}-${date.getFullYear()} ${hour}:${minute}`
}

function decorateProxyValues(json) {
    for (let item in json.list) {
        let TTL = getTimeToExpiration(json.list[item].date_end);
        json.list[item]["TTL"] = TTL[0];
        json.list[item]["TTL_hours"] = TTL[1];
        json.list[item]["TTL_minutes"] = TTL[2];
        json.list[item]["selected"] = false;
        json.list[item].date_end = transformDate(json.list[item].date_end);
        if (json.list[item].type === "socks") {
            json.list[item].type = "SOCKS5";
        } else if (json.list[item].type === "http") {
            json.list[item].type = "HTTPs";
        }
        if (json.list[item].version === "6") {
            json.list[item].version = "IPv6";
        } else if (json.list[item].version === "4") {
            json.list[item].version = "IPv4";
        } else if (json.list[item].version === "3") {
            json.list[item].version = "IPv4s";
        }

    }
}