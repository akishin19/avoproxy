import firebase from 'firebase'
import API from './api.js'

class User {
    constructor(apiKey, name, email, id, userKey = "") {
        this.apiKey = apiKey;
        this.name = name;
        this.balance = 0;
        this.proxyList = [];
        this.currency = `\u{20BD}`;
        this.email = email;
        this.id = id;
        this.userKey = userKey;
        this.api = new API(apiKey);
    }
}

function responseError(response, commit) {
    if (response.status === "no") {
        commit('setLoading', false);
        commit('setError', checkErrorCode(response.error_id));
        return true
    }
}

function checkErrorCode(code) {
    switch (code) {
        case 30:
            return "Your proxy alredy has selected protocol";
        case 100:
            return "Error ApiKey";
        case 250:
            return "Error description";
        case 300:
            return "Proxies ordered count is not available";
        case 400:
            return "Your balance is not enough funds";
    }
}

async function login(userId) {
    const requestUserData = await firebase.database().ref(`/users/`).once("value");
    const response = requestUserData.val();
    let tableId;
    for (let usr in response) {
        if (response[usr].id === userId.uid) {
            tableId = response[usr].userKey.key;
        }
    }
    return response[tableId];
}
export default {
    state: {
        user: null,
    },
    mutations: {
        setUser(state, payload) {
            state.user = payload;
        },
        setUsername(state, payload) {
            state.user.userKey = payload;
        },
        updateUserBalance(state, payload) {
            state.user.balance = payload;
        },
        createProxyList(state, payload) {
            state.user.proxyList = [];
            for (let proxy in payload) {
                state.user.proxyList.push(payload[proxy])
            }
        },
        updateProxyList(state, payload) {
            for (let proxy in payload) {
                state.user.proxyList.push(payload[proxy])
            }
        },
        selectProxyItem(state, payload) {
            for (let proxy of state.user.proxyList) {
                if (proxy.id === payload.proxyId) {
                    proxy.selected = payload.localSelected;
                }
            }
        },
        unselectAllItems(state) {
            for (let proxy of state.user.proxyList) {
                proxy.selected = false;
            }
        },
        deleteProxyItems(state, payload) {
            for (let i = 0; i < payload.length; i++) {
                let index = state.user.proxyList.findIndex(proxy => proxy.id === payload[i])
                state.user.proxyList.splice(index, 1)
            }
        },
        changeComment(state, payload) {
            let index = state.user.proxyList.findIndex(proxy => proxy.id === payload.proxyId)
            state.user.proxyList[index].descr = payload.newComment;
        },
        changeProtocol(state, payload) {
            for (let index of payload.ids) {
                state.user.proxyList[index].type = payload.protocol
            }
        },
        renewProxy(state, payload) {
            let pl = state.user.proxyList;
            state.user.balance = payload.balance;
            for (let index of payload.ids) {
                state.user.proxyList[index].TTL = payload.list[pl[index].id].TTL;
                state.user.proxyList[index].TTL_hours = payload.list[pl[index].id].TTL_hours;
                state.user.proxyList[index].TTL_minutes = payload.list[pl[index].id].TTL_minutes;
                state.user.proxyList[index].date_end = payload.list[pl[index].id].date_end;
                state.user.proxyList[index].unixtime_end = payload.list[pl[index].id].unixtime_end;
            }
        }
    },
    actions: {
        async registerUser({ dispatch, commit, getters }, { email, username, password }) {
            commit('clearError');
            commit('clearSuccess');
            commit('setLoading', true);
            try {
                const response = await firebase.auth().createUserWithEmailAndPassword(email, password);
                commit('setUser', new User('66bf711336-984bf1c504-b1fb81e03e', username, email, response.user.uid));
                const userKeyInDB = await firebase.database().ref(`/users/`).push(getters.user);
                const key = userKeyInDB.key;
                await firebase.database().ref(`users/${key}/userKey/`).update({ key });
                commit('setUsername', key);
                await dispatch("getProxy");
                commit('setLoading', false);
                commit('setSuccess', true);
            } catch (error) {
                commit('setLoading', false);
                commit('setError', error.message);
                throw error
            }
        },
        async loginUser({ commit, dispatch }, { email, password }) {
            commit('clearError');
            commit('setLoading', true);
            try {
                let authResult = await firebase.auth().signInWithEmailAndPassword(email, password);
                /* let user = await login(authResult.user);
                commit("setUser", new User(user.apiKey, user.name, user.email, user.id))
                await dispatch("getProxy");
                commit('setLoading', false); */
                dispatch('autoLoginUser', authResult)
                commit('setLoading', false);
            } catch (error) {
                commit('setLoading', false);
                commit('setError', error.message);
                throw error
            }
        },
        async autoLoginUser({ commit, dispatch }, payload) {
            commit('clearError');
            try {
                let user = await login(payload);
                if (!user) {
                    commit('setLoading', false);
                    return;
                }
                commit('setLoading', true);
                commit("setUser", new User(user.apiKey, user.name, user.email, user.id))
                await dispatch("getProxy");
                commit('setUsername', user.name);
                commit('setLoading', false);
            } catch (error) {
                commit('setLoading', false);
                commit('setError', error.message);
                throw error
            }
        },
        logoutUser({ commit }) {
            firebase.auth().signOut();
            commit("setUser", null);
        },
        async getProxy({ commit, getters }) {
            const response = await getters.user.api.getProxy();
            if (responseError(response, commit)) return
            commit("updateUserBalance", response.balance)
            commit("createProxyList", response.list)
        },
        async buyProxy({ commit, getters }, payload) {
            commit('clearError');
            commit('clearSuccess');
            commit('setLoading', true);
            try {
                const response = await getters.user.api.buy(payload.count, payload.period,
                    payload.country, payload.version, payload.protocol);
                if (responseError(response, commit)) return
                commit('setSuccess', true);
                commit('setLoading', false);
                for (let proxy in response.list) {
                    response.list[proxy]['country'] = payload.country;
                    response.list[proxy]['descr'] = '';
                }
                commit("updateUserBalance", response.balance)
                commit("updateProxyList", response.list)
            } catch (error) {
                commit('setLoading', false);
                commit('setError', error.message);
                throw error
            }
        },
        async deleteProxy({ commit, getters }, payload) {
            commit('clearError');
            commit('clearSuccess');
            try {
                const response = await getters.user.api.delete(payload);
                if (responseError(response, commit)) return
                commit("deleteProxyItems", payload)
                commit('setSuccess', true);
            } catch (error) {
                commit('setLoading', false);
                commit('setError', error.message);
                throw error
            }
        },
        async checkProxy({ commit, getters }, payload) {
            commit('clearError');
            commit('clearSuccess');
            try {
                const response = await getters.user.api.check(payload);
                if (responseError(response, commit)) return
                return response;
            } catch (error) {
                commit('setError', error.message);
                throw error
            }
        },
        async changeComment({ commit, getters }, payload) {
            commit('clearError');
            commit('clearSuccess');
            try {
                const response = await getters.user.api.setDescr(payload.newComment, payload.proxyId);
                if (responseError(response, commit)) return
                commit("changeComment", payload);
                commit('setSuccess', true);
            } catch (error) {
                commit('setError', error.message);
                throw error
            }
        },
        async changeProtocol({ commit, getters }, payload) {
            commit('clearError');
            commit('clearSuccess');
            try {
                let newProtocol;
                if (payload === "HTTPs") newProtocol = "http";
                else newProtocol = "socks";
                const response = await getters.user.api.setType(getters.selectedProxyIndexes.proxyIds, newProtocol);
                if (responseError(response, commit)) return
                commit("changeProtocol", { ids: getters.selectedProxyIndexes.proxyIndexes, protocol: payload });
                commit('setSuccess', true);
            } catch (error) {
                commit('setError', error.message);
                throw error
            }
        },
        async renewProxy({ commit, getters }, payload) {
            commit('clearError');
            commit('clearSuccess');
            try {
                let renewPeriod;
                if (payload === "3 Days") {
                    renewPeriod = 3
                } else if (payload === "1 Week") {
                    renewPeriod = 7
                } else if (payload === "2 Weeks") {
                    renewPeriod = 14
                } else renewPeriod = 30;
                const response = await getters.user.api.prolong(renewPeriod, getters.selectedProxyIndexes.proxyIds);
                if (responseError(response, commit)) return
                let renewResult = {
                    ids: getters.selectedProxyIndexes.proxyIndexes,
                    balance: response.balance,
                    list: response.list
                }
                commit("renewProxy", renewResult);
                commit('setSuccess', true);
            } catch (error) {
                commit('setError', error.message);
                throw error
            }
        },
    },
    getters: {
        user(state) {
            return state.user
        },
        proxyList(state) {
            return state.user.proxyList
        },
        isUserLoggedIn(state) {
            return state.user !== null
        },
        selectedProxyIndexes(state) {
            let proxyList = state.user.proxyList;
            let proxyIndexes = [];
            let proxyIds = [];
            for (let i = 0; i < proxyList.length; i++) {
                if (proxyList[i].selected === true) {
                    proxyIndexes.push(i);
                    proxyIds.push(proxyList[i].id)
                }
            }
            let result = {
                proxyIndexes,
                proxyIds
            }
            return result
        }
    }
}