import Vue from 'vue'
import Vuex from 'vuex'
import Vuetify from 'vuetify'
import App from './App'
import router from './router'
import firebase from 'firebase'
import store from './store/index.js'
import 'vuetify/dist/vuetify.min.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'

Vue.use(Vuex);
Vue.use(Vuetify);
Vue.config.productionTip = false

let firebaseConfig = {
    apiKey: "AIzaSyB79THt6Mtq6k7H1UImHr9oDq8RfbbfBNo",
    authDomain: "avo-proxy.firebaseapp.com",
    databaseURL: "https://avo-proxy.firebaseio.com",
    projectId: "avo-proxy",
    storageBucket: "avo-proxy.appspot.com",
    messagingSenderId: "177641117586",
    appId: "1:177641117586:web:7d5f9e63bf199f7698d34b"
};
firebase.initializeApp(firebaseConfig);

new Vue({
    el: '#app',
    router,
    store,
    vuetify: new Vuetify({
        theme: {
            themes: {
                dark: {
                    primary: '#632ad5',
                    secondary: '#b0bec5',
                    accent: '#8c9eff',
                    error: '#f44336',
                },
            },
        },
    }),
    components: { App },
    template: '<App/>',
    /* created() {
        firebase.auth().onAuthStateChanged(user => {
            if (user) {
                this.$store.dispatch('autoLoginUser', user)
            }
        });
    } */
})