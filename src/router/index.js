import Vue from 'vue'
import Router from 'vue-router'
import Home from '../components/Pages/Home.vue'
import AboutUs from '../components/Pages/AboutUs.vue'
/* import Buy from '../components/Pages/Buy.vue' */
import Contacts from '../components/Pages/Contacts.vue'
import FAQ from '../components/Pages/FAQ.vue'
/* import MyProxies from '../components/Pages/MyProxies.vue' */
import Prices from '../components/Pages/Prices.vue'
import Login from '../components/Pages/Auth/Login.vue'
import Registration from '../components/Pages/Auth/Registration.vue'
import PageNotFound from '../components/Pages/PageNotFound.vue'
import AuthGuard from './auth-guard.js'

Vue.use(Router)

const Buy = resolve => {
    require.ensure(['../components/Pages/Buy.vue'], () => {
        resolve(require('../components/Pages/Buy.vue'))
    })
}

const MyProxies = resolve => {
    require.ensure(['../components/Pages/MyProxies.vue'], () => {
        resolve(require('../components/Pages/MyProxies.vue'))
    })
}

export default new Router({
    routes: [{
        path: '',
        name: 'home',
        beforeEnter: AuthGuard,
        component: Home
    }, {
        path: '/about_us',
        name: 'about',
        beforeEnter: AuthGuard,
        component: AboutUs
    }, {
        path: '/buy',
        name: 'buy',
        beforeEnter: AuthGuard,
        component: Buy
    }, {
        path: '/contact_us',
        name: 'contact',
        beforeEnter: AuthGuard,
        component: Contacts
    }, {
        path: '/faq',
        name: 'faq',
        beforeEnter: AuthGuard,
        component: FAQ
    }, {
        path: '/my_proxies',
        name: 'my_proxies',
        beforeEnter: AuthGuard,
        component: MyProxies
    }, {
        path: '/prices',
        name: 'prices',
        beforeEnter: AuthGuard,
        component: Prices
    }, {
        path: '/login',
        name: 'login',
        beforeEnter: AuthGuard,
        component: Login
    }, {
        path: '/registration',
        name: 'registration',
        beforeEnter: AuthGuard,
        component: Registration
    }, {
        path: '/404',
        name: '404',
        beforeEnter: AuthGuard,
        component: PageNotFound,
    }, {
        path: '*',
        redirect: '/404'
    }],
    mode: 'history'
})