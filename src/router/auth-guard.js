import store from '../store/index.js'
import firebase from 'firebase'

export default function(to, from, next) {
    if (store.getters.user) {
        next()
    } else {
        let allowedRoutes = ["home", "about", "contact", "faq", "prices", "login", "registration", "404"];
        if (allowedRoutes.includes(to.name)) next()
        firebase.auth().onAuthStateChanged(user => {
            if (user) {
                store.dispatch('autoLoginUser', user).then(() => next())
                    .catch(() => {});
            } else if (allowedRoutes.includes(to.name)) next()
            else next('/login?loginError=true')
        })
    }
}